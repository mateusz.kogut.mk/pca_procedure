#!/usr/bin/env python
# -*- coding: utf-8 -*-
# comments

from optparse import OptionParser
from time import localtime, strftime
import numpy as np

def main():
    # Options parsing.
    parser = OptionParser(usage="%prog -i INPUT -p PCA -n NUMBERS [-o OUTPUT]", description="Description: %prog calculates projections of INPUT data on eigenvectors chosen from PCA data as NUMBERS.", epilog="Example: pca_projection.py -i input.txt -p pca_output.txt -n '0 1 3' -o output.txt")
    parser.add_option("-i", "--input", help="Space separated columns of numerical data (for projection).")
    parser.add_option("-p", "--pca", help="Space separated columns of numerical data (averages, eigenvalues and eigenvectors from pca).")
    parser.add_option("-n", "--numbers", help="Space separated string of eigenvectors numbers to be used for projection (starting from zero).")
    parser.add_option("-o", "--output", default="pca_projections.dat", help="Output data: projections of INPUT data on eigenvectors chosen from PCA data as NUMBERS.")
    (options, args) = parser.parse_args()
    # Input reading.
    data = read_file(options.input)
    print("[" + strftime("%H:%M:%S", localtime()) + "] INPUT data loaded.")
    pca = read_file(options.pca)
    print("[" + strftime("%H:%M:%S", localtime()) + "] PCA data loaded.")
    numbers = [int(n) + 2 for n in options.numbers.split()] #selecting appropriate eigenvectors from the user list
    # Calculations
    print("[" + strftime("%H:%M:%S", localtime()) + "] Calculations in progress...")
    output_projections = data_projection(np.array(data), np.array(pca)[:, numbers], np.array(pca)[:, 0])
    print("[" + strftime("%H:%M:%S", localtime()) + "] Done.")
    # Writing an output to a text file.
    np.savetxt(options.output, output_projections, fmt='%.18f')
    print("[" + strftime("%H:%M:%S", localtime()) + "] Output saved as:", options.output)

###################################################################################################

# Reads file as a float matrix.
def read_file(input_file):
    f = []
    with open(input_file) as fp:
        for line in fp:
            f.append(line.rstrip().split())
    for row in range(len(f)):
        for i in range(len(f[row])):
            f[row][i] = float(f[row][i])
    return f

###################################################################################################

# Calculates projections of data on eigenvectors.
def data_projection(data, eigenvectors, averages):
    data = data - averages.T
    projections = np.zeros((len(data), len(eigenvectors[0])))
    for data_row in range(len(data)):
        for eigenvector in range(len(eigenvectors[0])):
            projections[data_row, eigenvector] = np.dot(data[data_row], eigenvectors[:, eigenvector])
    return projections

###################################################################################################

main()