#!/usr/bin/env python
# -*- coding: utf-8 -*-
# comments

from optparse import OptionParser
from time import localtime, strftime
import numpy as np

def main():
    # Options parsing.
    parser = OptionParser(usage="%prog -i INPUT [-w WEIGHTS -o OUTPUT]", description="Description: %prog calculates (weighted) averages, eigenvalues and eigenvectors for principal component analysis.", epilog="Example: principal_component.py -i input.txt -w weights.txt -o output.txt")
    parser.add_option("-i", "--input", help="Space separated columns of numerical data.")
    parser.add_option("-o", "--output", default="pri_com_output.dat", help="Output data: averages (first column), eigenvalues (second column) and eigenvectors (other columns).")
    parser.add_option("-w", "--weights", help="Weights for input data (as a vector).")
    (options, args) = parser.parse_args()
    # Input reading.
    data = read_file(options.input)
    print("[" + strftime("%H:%M:%S", localtime()) + "] Data loaded.")
    if options.weights == None:
        print("[" + strftime("%H:%M:%S", localtime()) + "] Weights option not specified.")
        weights = [1 for x in range(len(data))]
        print("[" + strftime("%H:%M:%S", localtime()) + "] Default weights loaded: [{0}, {0}, {0}, ..., {0}]".format(weights[0]))
    else:
        weights = read_file(options.weights)
        print("[" + strftime("%H:%M:%S", localtime()) + "] Weights loaded.")
    # Calculations.
    print("[" + strftime("%H:%M:%S", localtime()) + "] Calculations in progress...")
    averages = weighted_average(data, weights)

    eigenvalues, eigenvectors = ordered_eig(data)
    output = concatenate_output(averages, eigenvalues, eigenvectors)
    print("[" + strftime("%H:%M:%S", localtime()) + "] Done.")
    # Writing output to a text file.
    np.savetxt(options.output, output.real, fmt='%.6f')     #only real part of numbers, imaginery parts can by ignored!!!
    print("[" + strftime("%H:%M:%S", localtime()) + "] Output saved as:", options.output)
    if any(t < 0 for t in averages):
        print()
        print("!!!!!!!!!!!!!!Possible reversal of the eigenvectors around the axis!!!!!!!!!!!!!!")
        print("!!!Mean values are negative!!!")

###################################################################################################

# Reads file as a float matrix.
def read_file(input_file):
    f = []
    with open(input_file) as fp:
        for line in fp:
            f.append(line.rstrip().split())
    for row in range(len(f)):
        for i in range(len(f[row])):
            f[row][i] = float(f[row][i])
    return f

###################################################################################################

# Computes vector of weighted averages (of every data column).
def weighted_average(data_matrix, weights):
    averages = []
    if isinstance(weights[1], list): # Convert weights to a list of numbers.
        for index in range(len(weights)):
            weights[index] = weights[index][0]
    rows = len(data_matrix)
    columns = len(data_matrix[1])
    for column in range(columns):
        average = 0
        for row in range(rows):
            average += data_matrix[row][column] * weights[row]
        averages.append(average/sum(weights))
    return averages

###################################################################################################

# Computes decreasingly ordered eigenvalues and corresponding eigenvectors.
def ordered_eig(data_matrix):
    eigenvalues, eigenvectors = np.linalg.eig(np.cov(data_matrix, rowvar=0))
    ordered_indices = eigenvalues.argsort()[::-1]
    return eigenvalues[ordered_indices], eigenvectors[:, ordered_indices]

###################################################################################################

# Concatenates averages, eigenvalues and eigenvectors.
def concatenate_output(averages, eigenvalues, eigenvectors):
    averages = np.transpose([averages])
    eigenvalues = np.transpose([eigenvalues])
    return np.concatenate((averages, eigenvalues, eigenvectors), axis=1)

###################################################################################################

main()