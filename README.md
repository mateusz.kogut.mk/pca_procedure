General PCA procedure:

1) prepare data, each observable in a separate column (e.g. data_all_DNA.dat)
2) if observables have different units or variance perform data standardization:
python data_standardization.py -i data_all_DNA.dat -o data_std_all_DNA.dat 
3) calculate mean values, eigenvalues and eigenvectors:
python principal_component.py -i data_std_all_DNA.dat
4) proprojection of all data for the most important eigenvectors:
python pca_projection.py -i data_std_all_DNA.dat -p pri_com_output.dat -n "0 1" -o project_all_DNA.dat