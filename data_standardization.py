#!/usr/bin/env python
# -*- coding: utf-8 -*-
# comments

from optparse import OptionParser
from time import localtime, strftime
from sklearn.preprocessing import StandardScaler
import numpy as np

def main():
    # Options parsing.
    parser = OptionParser(usage="%prog -i INPUT [-o OUTPUT]", description="Description: %prog standardizes dataset for principal component analysis.", epilog="Example: data_standardization.py -i data.dat -o data_std.dat")
    parser.add_option("-m", "--model", default="no", help="Input model for standardization: mean values and standard deviations in 2 rows.")
    parser.add_option("-i", "--input", help="Space separated columns of numerical data.")
    parser.add_option("-o", "--output", default="data_std.dat", help="Output data.")
    (options, args) = parser.parse_args()


    # Input data reading.
    data = read_file(options.input)
    print("[" + strftime("%H:%M:%S", localtime()) + "] Data loaded.")


    # Procedure of data standardization: subtracting the mean values and dividing by standard deviations
    if options.model == "no":
        model = StandardScaler().fit(data)  #getting a model for standardization
        whole_model = np.concatenate(([model.mean_], [model.scale_] ), axis=0) #concatenates means and standard deviations
        np.savetxt("model.dat", whole_model, fmt='%.6f')    #save model of standardization
        print("[" + strftime("%H:%M:%S", localtime()) + "] Model of standardization saved as: model.dat")
        data_standardized = model.transform(data)  # standardization according to the model
    else:
        model = read_file(options.model)    #read model from the file
        print("[" + strftime("%H:%M:%S", localtime()) + "] Model of standardization loaded")
        means = model[0]
        stds = model[1]
        data_means = np.subtract(data, means)
        data_standardized = data_means / stds


    # TODO: add standardization of circular data.


    np.savetxt(options.output, data_standardized, fmt='%.6f')
    print("[" + strftime("%H:%M:%S", localtime()) + "] Standardized data saved as:", options.output)

###################################################################################################

# Reads file as a float matrix.
def read_file(input_file):
    f = []
    with open(input_file) as fp:
        for line in fp:
            f.append(line.rstrip().split())
    for row in range(len(f)):
        for i in range(len(f[row])):
            f[row][i] = float(f[row][i])
    return f

###################################################################################################

main()